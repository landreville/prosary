
# Prosary Editor

Prosary is a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) editor for TiddlyWiki
based on [TipTap](https://tiptap.dev/).

The goal for this plug-in is to provide a visual editing component for all Tiddly WikiText markup.

To install go to https://landreville.gitlab.io/prosary/ and follow the directions.

To use Prosary, edit a new or existing Tiddler and change the Tiddler Type to
`Prosary Editor(text/vnd.prosary)`. This will convert the existing WikiText into the Prosary data structure.
The editor data is stored as JSON and will not be accessible via WikiText after converting.


# Technical Details

Two new widgets are created: Prosary and ProsaryView. Prosary is rendered when editing Tiddlers with
content-type "vnd.prosary" and ProsaryView is used when viewing.

The Prosary widget will render a VueJS application containing a [TipTap](https://tiptap.dev/) editor instance.

When a Tiddler is changed to the new vnd.prosary type the content will be parsed into the TipTap data structure.
The WikiText will be parsed into it's TiddlyWiki node structure using TiddlyWiki's native parsing. Then the
TW data is converted to TipTap data structure using parsers built-in to the new Prosary widget. The parsers
are in the lib/parser directory.

There are custom TipTap extensions to provide functionality for TiddlyWiki markup.

## TwBlock TipTap Extension

The TwBlock extension allows editing raw TiddlyWiki markup and viewing a preview in the editor. The preview
is rendered using TiddlyWiki's native rendering. The raw markup has to be re-created based on TiddlyWiki nodes,
because after WikiText is parsed there is no method to find the text that defined the TW nodes. This will result
in raw markup that is different from the initial input, but will be consistent. For example macrocalls are
always translated to `<$macrocall/>` format. These markup generators are in lib/parser/twparsers.

If the TiddlyWiki node does not have a markup generator in twparsers, then the TiddlyWiki node JSON is used
as the text to display. This can be rendered just like WikiText, ensuring that the view output is accurate. It will be
difficult to edit, because it's raw JSON.

## PrettyLink TipTap Extension

The PrettyLink extension implements the functionality necessary to open internal links to Tiddlers as well as
external links. It uses TiddlyWiki native functionality to open internal links.

# To Do

* Parse tables into TipTap table
* Create TipTap extensions for:
** Macro Call
** Transclude
** Image
** Widget call
** Import variables
** Set variables
** Define macro
* Create a toolbar button for PrettyLink
* Transform TipTap nodes into WikiText to save data as native WikiText.

# Development

To build call `build.sh` which will call `yarn build` to build the VueJS application and put it in the dist directory.
Then it will copy the distribution files into the plugin directory with appropriate TiddlyWiki headers to make 
them into Tiddlers for the TiddlyWiki plugin to use.

To run TiddlyWiki for plugin development:

1. Clone https://github.com/Jermolene/TiddlyWiki5.git 
2. Link the prosary/plugin directory to TW5/plugins/landreville/prosary
3. Run TiddlyWiki with `node tiddlywiki.js editions/tw5.com --listen`
4. Open your browser to http://localhost:8080


To deploy to a NodeJS TiddlyWiki installation copy the plugin folder to the plugins directory. For example:
```bash
rsync -avz plugin/* mywiki/plugins/prosary/
```

