#!/bin/bash
set -e

DISTDIR=plugin

yarn build

cat <<EOT > $DISTDIR/dist.js
/*\\
title: $:/plugins/landreville/prosary/dist.js
type: application/javascript
module-type: library
\*/

EOT

cat dist/prosary.umd.js >> $DISTDIR/dist.js

if [[ -f "dist/prosary.css" ]]; then
  cat <<EOT > $DISTDIR/styles.css
/*\\
title: $:/plugins/landreville/prosary/styles.css
tags: [[$:/tags/Stylesheet]]
\*/
EOT

  cat dist/prosary.css >> $DISTDIR/styles.css
fi

tiddlywiki demo --render "[[$:/plugins/landreville/prosary]] [[Prosary Demo]] [[Markup in WikiText]] [[$:/core/save/all]]" index.html text/plain
mv demo/output/index.html public/