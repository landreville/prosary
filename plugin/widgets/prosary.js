/*\
title: $:/plugins/landreville/prosary/widgets/prosary.js
type: application/javascript
module-type: library
\*/

;(function () {
    let Widget = require('$:/core/modules/widgets/widget.js').widget

    /**
     * The Prosary Widget renders the VueJS application containing
     * the TipTap editor.
     */
    let ProsaryWidget = function (parseTreeNode, options) {
        this.initialise(parseTreeNode, options)
    }

    ProsaryWidget.prototype = new Widget()

    ProsaryWidget.prototype.init = function (editable) {
        this.editable = editable === undefined ? true : editable
        // Title of current tiddler
        this.editTitle = this.getAttribute(
            'tiddler',
            this.getVariable('currentTiddler')
        )
        // Edit field name (wikitext edit field)
        this.editField = this.getAttribute('field', 'text')
        // Key to hold structured TipTap data from the WYSIWYG editor.
        this.dataFieldKey = 'prosarydata'
        this.holderClass = 'prosary__holder'
        // This Tiddler stores the configuration for the editor.
        this.config = $tw.wiki.getTiddlerData(
            '$:/plugins/landreville/prosary/config.json',
            {}
        )
    }
    /**
     * Return TipTap data if it has been saved on this Tiddler already.
     * If there is none yet, then returns null.
     */
    ProsaryWidget.prototype.getStructuredData = function () {
        let tiddler = this.wiki.getTiddler(this.editTitle)
        let data = tiddler.fields[this.dataFieldKey]
        if (!data) {
            data = null
        } else {
            data = JSON.parse(data)
        }
        return data
    }
    /**
     * Render the ProsaryWidget in the Tiddler.
     */
    ProsaryWidget.prototype.render = function (parent, nextSibling) {
        this.init()
        this.parentDomNode = parent
        // VueJS App will be mounted in this div
        let widgetNode = document.createElement('div')
        widgetNode.className = this.holderClass
        this.setupEditor(widgetNode)

        if (nextSibling) {
            parent.insertBefore(widgetNode, nextSibling)
        } else {
            // If there's no sibling given, then render in the parent.
            try {
                parent.appendChild(widgetNode)
            } catch (e) {
                // If there's no sibling and no parent then...
                console.log('Could not append to parent.')
            }
        }
        this.domNodes.push(widgetNode)
    }

    /**
     * Instantiate and mount the VueJS app containing the TipTap editor.
     */
    ProsaryWidget.prototype.setupEditor = function (widgetNode) {
        // The VueJS App distribution
        let distLib = require('$:/plugins/landreville/prosary/dist.js')
        // It has an entry point that exports "app" as the function
        // to start the app.
        let initVueJSApp = distLib.app
        let app = initVueJSApp()
        // eslint-disable-next-line no-undef
        // Provide TiddlyWiki globals to the VueJS app
        app.provide('tw', $tw)
        app.provide('wiki', this.wiki)
        app.provide('widget', this)
        app.provide('editable', this.editable)
        // app.config.globalProperties.$tw = $tw;
        // app.config.globalProperties.$wiki = this.wiki;
        // app.config.globalProperties.$widget = this;
        app.mount(widgetNode)
    }

    ProsaryWidget.prototype.save = function (data, renderedHtml) {
        let tiddler = this.wiki.getTiddler(this.editTitle)

        // The TipTap structured data is stored as a property of the Tiddler.
        // Tiddler properties must be serializable.

        let tiddlerData = {
            title: this.editTitle,
            [this.editField]: '<$prosary/>',
            [this.dataFieldKey]: JSON.stringify(data),
        }

        this.wiki.addTiddler(
            new $tw.Tiddler(
                this.wiki.getCreationFields(),
                tiddler,
                tiddlerData,
                this.wiki.getModificationFields()
            )
        )
    }

    /**
     * The ProsaryViewWidget is the ProsaryWidget with editable
     * set to false to make the editor read-only.
     */
    let ProsaryViewWidget = function (parseTreeNode, options) {
        this.initialise(parseTreeNode, options)
    }

    ProsaryViewWidget.prototype = new ProsaryWidget()

    ProsaryViewWidget.prototype.init = function () {
        // Set editable to false
        ProsaryWidget.prototype.init.call(this, false)
    }

    // Allow TipTap to handle clicks on the PrettyLink components
    document.addEventListener('click', (event) => {
        if (event.target.getAttribute('data-link-type') === 'prettyLink') {
            event.preventDefault()
        }
    })

    exports.ProsaryWidget = ProsaryWidget
    exports.ProsaryViewWidget = ProsaryViewWidget
})()
