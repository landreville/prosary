/*\
title: $:/plugins/landreville/prosary/prosary.js
type: application/javascript
module-type: widget
\*/

if ($tw.browser) {
    ;(function () {
        'use strict'
        let mappings = require('$:/plugins/landreville/prosary/mappings.js')

        let widgets = mappings.makeEditorMappings({ wiki: $tw.wiki })
        // Export the view and edit widget
        for (const [key, widgetClass] of Object.entries(widgets)) {
            exports[key] = widgetClass
        }
    })()
}
