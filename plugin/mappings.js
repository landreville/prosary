/*\
title: $:/plugins/landreville/prosary/mappings.js
type: application/javascript
module-type: library
\*/

;(function () {
    /**
     *  Create editor-type mappings. This will ensure that the ProsaryWidget
     *  is used for editing Tiddlers with a specific content-type "vnd.prosary".
     */

    let prosaryWidget = require('$:/plugins/landreville/prosary/widgets/prosary.js')

    const mappings = [
        {
            docTitle: '$:/language/Docs/Types/text/vnd.prosary',
            mappingTitle: '$:/config/EditorTypeMappings/text/vnd.prosary',
            description: 'Prosary Editor',
            name: 'text/vnd.prosary',
            group: 'Text',
            widgetName: 'prosary',
            editWidget: prosaryWidget.ProsaryWidget,
            viewWidget: prosaryWidget.ProsaryViewWidget,
        },
    ]

    exports.makeEditorMappings = function ({ wiki }) {
        let widgetMappings = {}

        for (const mapping of mappings) {
            // Add editor-type mapping if it doesn't exist
            let typeMapping = wiki.getTiddler(mapping.mappingTitle)
            if (typeMapping === undefined) {
                wiki.addTiddler(
                    new $tw.Tiddler($tw.wiki.getCreationFields(), {
                        title: mapping.mappingTitle,
                        text: mapping.widgetName,
                    })
                )
            }

            // Add editor-type mapping doc Tiddler
            let typeDoc = wiki.getTiddler(mapping.docTitle)
            if (typeDoc === undefined) {
                wiki.addTiddler(
                    new $tw.Tiddler($tw.wiki.getCreationFields(), {
                        title: mapping.docTitle,
                        description: mapping.description,
                        name: mapping.name,
                        group: mapping.group,
                    })
                )
            }

            // Map the ProsaryWidget instance to the widget name.
            // By convention "edit-" is prepended for the widget to use
            // when editing.
            if (mapping.widgetName !== undefined) {
                widgetMappings[mapping.widgetName] = mapping.viewWidget
                widgetMappings[`edit-${mapping.widgetName}`] =
                    mapping.editWidget
            }
        }

        return widgetMappings
    }
})()
