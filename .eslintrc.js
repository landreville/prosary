module.exports = {
    root: true,

    env: {
        browser: true,
        node: true,
    },

    parserOptions: {
        ecmaVersion: 2020,
        parser: '@typescript-eslint/parser',
    },
    rules: {
        // indent: ['error', 4],
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        semi: [2, 'never'],
        curly: 2,
        eqeqeq: 2,
        quotes: [2, 'single', { avoidEscape: true }],
        'prettier/prettier': ['error', { semi: false, singleQuote: true }],
        '@typescript-eslint/no-this-alias': [
            'error',
            {
                allowedNames: ['self'], // Allow `const self = this`; `[]` by default
            },
        ],
        '@typescript-eslint/no-explicit-any': ['off'],
        '@typescript-eslint/explicit-module-boundary-types': ['off'],
    },

    extends: [
        'plugin:vue/essential',
        'eslint:recommended',
        '@vue/typescript/recommended',
        '@vue/prettier',
        '@vue/prettier/@typescript-eslint',
        '@vue/typescript',
    ],
}
