interface TipTapNode {
    type: string
    attrs?: Record<string, any>
    content?: TipTapNode[]
    text?: string
    marks?: { type: string }[]
}
