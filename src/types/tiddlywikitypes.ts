export interface TwNode {
    type: string
    tag: string
    children: TwNode[]
    attributes?: Record<string, any>
    value?: string
    text?: string
}

export type Wiki = any
export type TW = any
export type Widget = any
