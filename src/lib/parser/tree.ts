/**
 * Functions that help with parsed WikiText node trees.
 */

import { getParser } from '@/lib/parser/parsers'
import { TwNode } from '@/types/tiddlywikitypes'
import { getTwParser } from '@/lib/parser/twparsers'

/**
 * Parse a WikiText node into the appropriate TipTap nodes.
 */
export function parseNode(node: TwNode): TipTapNode[] | null {
    const blockParser = getParser(node)
    if (blockParser !== null) {
        return blockParser(node)
    }

    console.log(`No parser found for ${node.type}`)
    return null
}

/**
 * Parse all of the TiddlyWiki node children into an array of
 * TipTap nodes.
 */
export function parseChildren(node: TwNode): TipTapNode[] {
    if (!node.children) {
        return []
    }
    let parsedNodes: TipTapNode[] = []
    for (const child of node.children) {
        const newParsedNodes = parseNode(child)
        if (newParsedNodes) {
            parsedNodes = parsedNodes.concat(newParsedNodes)
        }
    }
    return parsedNodes
}

/**
 * Similar to flattenToText, but only uses parsers that return TwBlocks
 * and extracts the WikiText markup and returns it.
 * @param node
 */
export function parseChildrenToMarkup(node: TwNode): string {
    const texts: string[] = []
    let nodeTexts: string[] = []
    let twParser = null
    if (!node.children) {
        return ''
    }
    for (const child of node.children) {
        twParser = getTwParser(child)
        if (twParser !== null) {
            const parsedNodes = twParser(child)
            for (const parsedNode of parsedNodes) {
                if (parsedNode.content) {
                    nodeTexts = []
                    for (const content of parsedNode.content) {
                        if (content.type === 'text' && content.text) {
                            nodeTexts.push(content.text)
                        }
                    }
                    // Join text nodes within a parent with no spaces
                    texts.push(nodeTexts.join(''))
                }
            }
        }
    }
    // Join text from separate nodes with newline
    return texts.join('\n')
}
/**
 * Convert a tree of WikiText nodes into text.
 * This only returns text. No markup, no structure, just string.
 */
export function flattenToText(node: TwNode): string {
    const texts = []

    if (node.text) {
        texts.push(node.text)
    }

    if (node.children) {
        node.children.map((childNode) => texts.push(flattenToText(childNode)))
    }

    return texts.join(' ')
}

/**
 * Return list of "text"-type TipTap nodes.
 * This flattens TiddlyWiki nodes into text-only TipTap nodes.
 * It's useful when a TipTap node only allows text* content.
 */
export function flattenToTipTapText(node: TwNode): TipTapNode[] {
    const texts: TipTapNode[] = []

    const addText = (subNode: TipTapNode) => {
        if (subNode.text) {
            texts.push({
                type: 'text',
                text: subNode.text,
                marks: subNode.marks ? subNode.marks : [],
            })
        }
        if (subNode.content) {
            subNode.content.map(addText)
        }
    }

    for (const child of node.children) {
        const parser = getParser(child)
        if (parser !== null) {
            const parsedNodes = parser(child)
            parsedNodes.map(addText)
        }
    }

    return texts
}
