import { nodeParsers } from '@/lib/parser/parsers'

/**
 * Parse TiddlyWikiText attributes from the data structure and
 * output as an array of Strings suitable for including in macrocall markup.
 *
 * The attribute structure comes from TiddlyWiki and looks like:
 * ```
 * [ { "name": "my-attribute-name", "type": "string", "value": "my value" } ]
 * ```
 * The attribute types supported are:
 * - string -- Eg "my value"
 * - macro-parameter -- same as string, but used in a macro call
 * - filtered -- Eg `{{{ [filter[op]] }}}`
 * - indirect -- A field from a Tiddler such as: `{{MyTiddler!!field}}`
 * - macro -- A macrocall as a parameter such as: `<<myMacro myparam="myvalue">>`
 *
 * @param attributes - TiddlyWiki attributes structure.
 * @param sep - separator to use when creating output string. Default is `=`.
 */
export function parseAttributes(attributes: any, sep: string) {
    if (sep === undefined) {
        sep = '='
    }
    const paramValues = []
    for (const param of attributes) {
        let prefix = ''
        if (param.name) {
            prefix = `${param.name}${sep}`
        }
        if (param.type === 'macro-parameter' || param.type === 'string') {
            let paramValue = `"${param.value}"`
            if (param.value.includes('"')) {
                if (!param.value.includes("'")) {
                    paramValue = `'${param.value}'`
                } else if (!param.value.includes('"""')) {
                    paramValue = `"""${param.value}"""`
                }
            }
            paramValues.push(`${prefix}${paramValue}`)
        } else if (param.type === 'filtered') {
            // filtered params such as {{{ [filter[op]] }}}
            paramValues.push(`${prefix}{{{${param.filter}}}}`)
        } else if (param.type === 'indirect') {
            // indirect params such as {{MyTiddler!!field}}
            paramValues.push(`${prefix}{{${param.textReference}}`)
        } else if (param.type === 'macro') {
            // macro params such as <<myMacro myparam="myvalue">>
            paramValues.push(`${prefix}${nodeParsers.macrocall(param.value)}`)
        }
    }
    return paramValues.join(' ')
}
