/**
 * Parsers in this module always return TwBlock with wiki markup to
 * display in the editor. They have to take the TiddlyWiki nodes and
 * create the equivalent TiddlyWikiText that would have created them.
 *
 * As these are converted to custom TipTap nodes they can be moved to the
 * parsers module.
 */
import { parseAttributes } from '@/lib/parser/params'
import { TwNode } from '@/types/tiddlywikitypes'
import { parseChildren, parseChildrenToMarkup } from '@/lib/parser/tree'

/**
 * Return a parser appropriate for the given TiddlyWiki Node.
 */
export function getTwParser(node: TwNode) {
    if (Object.prototype.hasOwnProperty.call(parsers, node.type)) {
        return parsers[node.type]
    }

    if (node.tag) {
        if (node.tag.startsWith('$')) {
            return parsers.widget
        }
        return genericTag
    }

    return null
}

const parsers: Record<string, (node: any) => TipTapNode[]> = {
    image: (node) => {
        const src = node.attributes.source.value
        const markup = `[img[${src}]]`
        return [
            {
                type: 'twBlock',
                attrs: { twNode: node },
                content: [{ type: 'text', text: markup }],
            },
        ]
    },

    importvariables: (node) => {
        node.tag = '$importvariables'
        return parsers.widget(node)
    },

    macrocall: (node) => {
        const paramValues = []
        // Simple macro call such as <<mymacro one="myvalue">>
        // Use node.name and node.params
        let macroName = node.name
        let params = node.params
        let useSimpleMacro = true
        let sep = ':'
        // Macros called with the  $macrocall widget
        if (node.tag && node.tag === '$macrocall') {
            useSimpleMacro = false
            sep = '='
            macroName = node.attributes['$name']
            params = Object.values(node.attributes).filter(
                (el: any) => el.name !== '$name'
            )
        }
        const paramString = parseAttributes(params, sep)
        let macroCall = `<$macrocall $name="${macroName}" ${paramString}/>`
        if (useSimpleMacro) {
            macroCall = `<<${macroName} ${paramString}>>`
        }
        return [
            {
                type: 'twBlock',
                attrs: { twNode: node },
                content: [{ type: 'text', text: macroCall }],
            },
        ]
    },
    set: (node) => {
        if (node.isMacroDefinition) {
            // This is an example of macro definition node
            // {
            //     type: 'set',
            //     attributes: {
            //         name: { type: 'string', value: 'italicise' },
            //         value: { type: 'string', value: '//$text$//' },
            //     },
            //     children: [],
            //     params: [{ name: 'text' }],
            //     isMacroDefinition: true,
            // }
            // The children is the entire Tiddler contents.
            // The children are being extracted and placed in
            // sequence after the macro definition node.
            // Any TipTap custom extension that uses TiddlyWiki
            // rendering will need to access all macro definitions and
            // include them when rendering.
            const name = node.attributes.name.value
            const body = node.attributes.value.value
            const params = node.params
            const paramStrings = []
            for (const param of params) {
                paramStrings.push(param.name)
            }
            const paramString = paramStrings.join(', ')

            const wikiText = `\\define ${name}(${paramString})\n${body}\n\\end`

            return [
                {
                    type: 'macrodefinition',
                    attrs: {
                        name,
                        body,
                        params,
                    },
                    content: [{ type: 'text', text: wikiText }],
                },
                ...parseChildren(node),
            ]
        } else {
            // TODO: implement set variable
            return passThroughParser(node)
        }
    },

    tiddler: (node) => {
        // Example:
        //     type: 'tiddler',
        //     attributes: {
        //         tiddler: { type: 'string', value: 'Product Hunt Link' },
        //     },
        //     children: [
        //         {
        //             type: 'transclude',
        //             attributes: {
        //                 tiddler: { type: 'string', value: 'Product Hunt Link' },
        //             },
        //         },
        //     ],
        // }
        const targetTitle = node.attributes.tiddler.value
        const transcludeNode = node.children[0]
        let tiddler = null
        if (transcludeNode.attributes.tiddler) {
            tiddler = transcludeNode.attributes.tiddler.value
        }
        let field = null
        if (transcludeNode.attributes.field) {
            field = transcludeNode.attributes.field.value
        }
        let propertyIndex = null
        if (transcludeNode.attributes.index) {
            propertyIndex = transcludeNode.attributes.index.value
        }
        let template = null
        if (targetTitle !== tiddler) {
            template = tiddler
        }
        const parts = ['{{']
        if (tiddler) {
            parts.push(targetTitle)
        }
        if (field) {
            parts.push(`!!${field}`)
        }
        if (propertyIndex) {
            parts.push(`##${propertyIndex}`)
        }
        if (template) {
            parts.push(`|${template}`)
        }
        parts.push('}}')

        return [
            {
                type: 'twBlock',
                attrs: { twNode: node },
                content: [{ type: 'text', text: parts.join('') }],
            },
        ]
    },

    widget: (node) => {
        // Widget call such as <$widgetName>
        if (node.tag === '$macrocall') {
            return parsers.macrocall(node)
        }
        const widgetCall = [`<${node.tag}`]
        const paramString = parseAttributes(Object.values(node.attributes), '=')
        widgetCall.push(paramString)
        if (node.children) {
            widgetCall.push('>')
            // Because this is a TW widget call with children
            // all the children need to be TW blocks written as markup
            widgetCall.push(parseChildrenToMarkup(node))
            widgetCall.push(`</${node.tag}>`)
        } else {
            widgetCall.push('/>')
        }
        const markup = widgetCall.join(' ')
        return [
            {
                type: 'twBlock',
                attrs: { twNode: node, markup: markup },
                content: [{ type: 'text', text: markup }],
            },
        ]
    },
}

function genericTag(node: TwNode): TipTapNode[] {
    if (node.tag === 'br') {
        // Shortcut for newlines so that <br> isn't in the output
        return [{ type: 'paragraph', content: [] }]
    }

    const attrs = []
    if (node.attributes) {
        for (const [key, attr] of Object.entries(node.attributes)) {
            if (attr.type !== 'string') {
                continue
            }
            attrs.push(`${attr.name ? attr.name : key}="${attr.value}"`)
        }
    }
    // Don't put a space after the tag name if there is no attributes
    let attrString = `${attrs.join(' ')}`
    if (attrString.length > 0) {
        attrString = ' ' + attrString
    }
    const childContent = parseChildrenToMarkup(node)
    // Can't use createElement, because this could be non-html wikitext
    // widgets
    const content = `<${node.tag}${attrString}>
    ${childContent}
</${node.tag}>`

    return [
        {
            type: 'twBlock',
            attrs: { twNode: node },
            content: [{ type: 'text', text: content }],
        },
    ]
}

/**
 * Returns the TiddlyWikiNode JSON as the markup, because
 * we don't know how to convert from TW Node to WikiText for the
 * given TiddlyWiki node.
 *
 * This ensures that the data is not lost and will render correctly when
 * viewing the Tiddler. But it will not be easy to edit the content.
 *
 * Ideally this will never need to be used.
 */
function passThroughParser(node: TwNode) {
    return [
        {
            type: 'twBlock',
            attrs: { twNode: node },
            content: [{ type: 'text', text: JSON.stringify(node) }],
        },
    ]
}
