/**
 * Parse various HTML tags from the TiddlyWiki data structure
 * into the TipTap data structure.
 */
import {
    flattenToText,
    flattenToTipTapText,
    parseChildren,
    parseChildrenToMarkup,
} from '@/lib/parser/tree'
import { TwNode } from '@/types/tiddlywikitypes'
import { getTwParser } from '@/lib/parser/twparsers'

export const nodeParsers: Record<string, (node: any) => TipTapNode[]> = {
    blockquote: (node) => {
        return [
            {
                type: 'blockquote',
                content: parseChildren(node),
            },
        ]
    },

    codeblock: (node) => {
        return [
            {
                type: 'codeBlock',
                content: [{ type: 'text', text: node.attributes.code.value }],
            },
        ]
    },

    // This should be an inline parser
    // entity: (node) => {
    //   let el = document.createElement("span")
    //   el.innerHTML = node.entity
    //   return el.innerText
    // },

    div: (node) => {
        const attrs: any = {}
        if (node.attributes) {
            const entries: any[] = Object.entries(node.attributes)
            for (const [key, attr] of entries) {
                if (attr.type !== 'string') {
                    console.log(`Unknown attribute type: ${attr.type}`)
                    continue
                }
                attrs[attr.name] = attr.value
            }
        }

        const childContent = parseChildren(node)
        return [
            {
                type: 'div',
                content: childContent,
                attrs: attrs,
            },
        ]
    },

    p: (node) => {
        // Don't allow div nodes as a child of paragraph
        const returnNodes: TipTapNode[] = []
        let childNodes: TipTapNode[] = []

        for (const childNode of parseChildren(node)) {
            // TipTap paragraph does not allow block content
            if (childNode.type === 'div' || childNode.type === 'twBlock') {
                if (childNodes.length > 0) {
                    returnNodes.push({
                        type: 'paragraph',
                        content: childNodes,
                    })
                    childNodes = []
                }
                returnNodes.push(childNode)
            } else {
                childNodes.push(childNode)
            }
        }

        if (childNodes.length > 0) {
            returnNodes.push({
                type: 'paragraph',
                content: childNodes,
            })
        }

        return returnNodes
    },

    ul: (node) => {
        const content: TipTapNode[] = parseChildren(node)
        return [
            {
                type: 'bulletList',
                content,
            },
        ]
    },
    ol: (node) => {
        const content: TipTapNode[] = parseChildren(node)
        return [
            {
                type: 'orderedList',
                content,
            },
        ]
    },

    li: (node) => {
        const naiveContent = parseChildren(node)
        const content = []
        for (const el of naiveContent) {
            if (el.type === 'text') {
                // List item can only have Paragraph or block content
                content.push({
                    type: 'paragraph',
                    content: [el],
                })
            } else {
                content.push(el)
            }
        }
        return [
            {
                type: 'listItem',
                content: content,
            },
        ]
    },

    /**
     * Parse an anchor element node into anchor html.
     */
    a: (node: TwNode) => {
        let href = ''
        if (
            node.attributes &&
            node.attributes.href &&
            node.attributes.href.value
        ) {
            href = node.attributes.href.value
        }
        return [
            {
                type: 'text',
                text: flattenToText(node),
                marks: [{ type: 'prettyLink', attrs: { href } }],
            },
        ]
    },

    header: (node: TwNode) => {
        return [
            {
                type: 'heading',
                attrs: {
                    // Number of the header level
                    level: parseInt(node.tag[1]),
                },
                content: parseChildren(node),
            },
        ]
    },

    cite: (node: TwNode) => {
        return [
            {
                type: 'blockquote',
                content: parseChildren(node),
            },
        ]
    },
    em: (node: TwNode) => {
        return [
            {
                type: 'text',
                text: flattenToText(node),
                marks: [{ type: 'italic' }],
            },
        ]
    },
    strong: (node: TwNode) => {
        return [
            {
                type: 'text',
                text: flattenToText(node),
                marks: [{ type: 'bold' }],
            },
        ]
    },
    strike: (node: TwNode) => {
        return [
            {
                type: 'text',
                text: flattenToText(node),
                marks: [{ type: 'strike' }],
            },
        ]
    },
    link: (node: TwNode) => {
        let href = ''
        if (node.attributes && node.attributes.to && node.attributes.to.value) {
            href = node.attributes.to.value
        }

        return [
            {
                type: 'text',
                text: flattenToText(node),
                marks: [{ type: 'prettyLink', attrs: { href } }],
            },
        ]
    },

    string: (node: TwNode) => {
        return [{ type: 'text', text: node.value }]
    },

    text: (node: TwNode) => {
        return [{ type: 'text', text: node.text }]
    },
}

/**
 * Return the parser function appropriate for the given TiddlyWiki node.
 *
 * If no parser is found, then returns null.
 */
export function getParser(node: TwNode) {
    if (Object.prototype.hasOwnProperty.call(nodeParsers, node.type)) {
        return nodeParsers[node.type]
    }

    if (node.tag) {
        if (Object.prototype.hasOwnProperty.call(nodeParsers, node.tag)) {
            return nodeParsers[node.tag]
        }
        if (node.tag.match(/h[1-9]/)) {
            return nodeParsers.header
        }
    }

    const twParser = getTwParser(node)
    if (twParser !== null) {
        return twParser
    }

    return null
}
