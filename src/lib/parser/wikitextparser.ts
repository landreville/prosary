/**
 * Parse TiddlyWikiText into TiddlyWiki nodes and then into TipTap nodes.
 */
import { parseNode } from './tree'

export class WikiTextParser {
    wiki: any
    parseAsInline: boolean

    constructor({ wiki }: { wiki: any }) {
        this.wiki = wiki
        this.parseAsInline = false
    }

    /**
     * Parse WikiText string into TipTap nodes.
     */
    parseWikiText(wikitext: string) {
        // Use TiddlyWiki to parse the WikiText
        const parser = this.wiki.parseText('text/vnd.tiddlywiki', wikitext, {
            parseAsInline: this.parseAsInline,
        })
        const tree = parser.tree
        console.log('TiddlyWiki Data', tree)
        // Parse TiddlyWiki nodes into TipTap nodes
        const tiptapData = { type: 'doc', content: this.parseTree(tree) }
        console.log('TipTap Data', tiptapData)
        return tiptapData
    }

    parseTree(tree: any): TipTapNode[] {
        let nodes: TipTapNode[] = []
        for (const node of tree) {
            const newNodes = parseNode(node)
            if (newNodes !== null) {
                nodes = nodes.concat(newNodes)
            }
        }
        return nodes
    }
}
