/**
 * TipTap extension that allows editing raw HTML and will render
 * the HTML when viewing.
 */
import { Node, mergeAttributes } from '@tiptap/core'

export interface DivOptions {
    HTMLAttributes: Record<string, any>
}

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
        div: {
            setDiv: () => ReturnType
        }
    }
}

export const Div = Node.create<DivOptions>({
    name: 'div',
    priority: 1000,
    group: 'block',
    content: 'inline*',

    addOptions() {
        return {
            HTMLAttributes: {},
        }
    },

    parseHTML() {
        return [{ tag: 'div[data-type="div"]' }]
    },

    renderHTML({ HTMLAttributes }) {
        return [
            'div',
            mergeAttributes(this.options.HTMLAttributes, HTMLAttributes, {
                'data-type': 'div',
            }),
            0,
        ]
    },

    addCommands() {
        return {
            setDiv:
                () =>
                ({ commands }) => {
                    return commands.setNode('div')
                },
        }
    },
})
