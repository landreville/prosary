/**
 * TipTap extension to edit raw TiddlyWikiText.
 * See TwBlockView for the VueJS component that renders the data.
 */
import { Node } from '@tiptap/core'
import { mergeAttributes, VueNodeViewRenderer } from '@tiptap/vue-3'
import TwBlockView from '@/components/TwBlockView.vue'
import { flattenToMarkup, renderTwWikitext } from '@/lib/twrender'

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
        twBlock: {
            setTwBlock: () => ReturnType
        }
    }
}

/**
 * Dynamically create a TwBlock extension. This changes the class structured
 * depending on whether useNodeView is true.
 *
 * If useNodeView is true, then rendering will be done by using TwBlockView
 * (through TipTap's VueNodeViewRenderer). Otherwise the TwBlock will
 * be rendered as static HTML.
 *
 * useNodeView should only be set to false if the editor is read-only or
 * you are exporting the Tiddler as a static HTML page.
 */
export function makeTwBlockExtension(useNodeView: boolean) {
    let extra = {}
    if (useNodeView) {
        // The node view does not need to be used if the editor is read-only.
        // The node view provides a live view of the wikitext and the
        // wikitext can only change when editing.
        extra = {
            addNodeView() {
                // Do not pass an options object with update defined to
                // VueNodeViewRenderer or it will not update props.node on the
                // Vue component
                const nodeView = VueNodeViewRenderer(TwBlockView)
                return nodeView
            },
        }
    }

    const TwBlock = Node.create({
        name: 'twBlock',
        content: 'text*',
        marks: '',
        group: 'block',
        code: true,
        defining: true,

        parseHTML() {
            return [
                {
                    tag: 'div[data-type="twBlock"]',
                },
            ]
        },

        renderHTML({ HTMLAttributes, node }) {
            // When editing this method is not called on updates.
            // The "0" in the output is replaced with the text typed
            // into the editor without re-rendering. Updating the HTML
            // output while editing requires using a NodeView.
            // This renderHTML function is only defined, because it is handy
            // for the read-only view and to have a method to output
            // static HTML for display later (such as deploying the
            // output as a static site).
            const wiki = this.options.wiki as any
            const macroDefinitions = this.options.macroDefinitions || ''

            const wikiText = flattenToMarkup(node)
            const renderedHtml = renderTwWikitext(
                wiki,
                macroDefinitions,
                wikiText
            )
            const el = document.createElement('div')
            el.innerHTML = renderedHtml

            return [
                'div',
                mergeAttributes(HTMLAttributes, { 'data-type': 'twBlock' }),
                el,
            ]
        },

        addAttributes() {
            return {
                twNode: {
                    default: null,
                },
                markup: {
                    default: '',
                },
            }
        },

        addCommands() {
            return {
                setTwBlock:
                    () =>
                    ({ commands }) => {
                        return commands.setNode('twBlock')
                    },
            }
        },

        addKeyboardShortcuts() {
            return {
                'Mod-Alt-t': () => this.editor.commands.setTwBlock(),

                // remove block when at start of document or code block is empty
                Backspace: () => {
                    const { empty, $anchor } = this.editor.state.selection
                    const isAtStart = $anchor.pos === 1

                    if (!empty || $anchor.parent.type.name !== this.name) {
                        return false
                    }

                    if (isAtStart || !$anchor.parent.textContent.length) {
                        return this.editor.commands.clearNodes()
                    }

                    return false
                },
            }
        },
        ...extra,
    })

    return TwBlock
}
