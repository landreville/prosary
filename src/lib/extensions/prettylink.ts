/**
 * This extension implements the equivalent of the TiddlyWiki PrettyLink widget.
 *
 * It can be used for internal links to Tiddlers or external links.
 */
import {
    Mark,
    markPasteRule,
    markInputRule,
    mergeAttributes,
} from '@tiptap/core'
import { Plugin, PluginKey } from 'prosemirror-state'
import { TW, Widget, Wiki } from '@/types/tiddlywikitypes'

export interface PrettyLinkOptions {
    /**
     * If enabled, links will be opened on click.
     */
    openOnClick: boolean
    /**
     * Adds a link to the current selection if the pasted content only contains an url.
     */
    linkOnPaste: boolean
    /**
     * A list of HTML attributes to be rendered.
     */
    HTMLAttributes: Record<string, any>
    tw: TW
    wiki: Wiki
    widget: Widget
}

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
        link: {
            /**
             * Set a link mark
             */
            setPrettyLink: (attributes: {
                href: string
                target?: string
            }) => ReturnType
            /**
             * Toggle a link mark
             */
            togglePrettyLink: (attributes: {
                href: string
                target?: string
            }) => ReturnType
            /**
             * Unset a link mark
             */
            unsetPrettyLink: () => ReturnType
        }
    }
}

/**
 * Regex matches links such as [[Label|URL]] or [[URL]]
 * The URL can be an external link or an internal wiki link.
 */
export const inputRegex = /(?:^|\s)\[\[(.*?)(?:\|(.*?))?\]\]$/gm
export const pasteRegex = /\[\[(.*?)(?:\|(.*?))?\]\]/gm
export const pasteRegexExact = /^\[\[(.*?)(?:\|(.*?))?\]\]$/gm

/**
 * Return true if the link is not to an internal page.
 */
function isExternal(href: string): boolean {
    return href.indexOf('://') !== -1
}

/**
 * Return a list of classes to include on the anchor.
 * This helps styling anchors differently if they are external, to shadow
 * tiddlers, etc.
 */
function buildClassList(wiki: Wiki, href: string): string[] {
    if (isExternal(href)) {
        return ['tc-tiddlylink-external']
    }
    // These are the CSS classes TiddlyWiki uses
    const isMissing = !wiki.tiddlerExists(href)
    const isShadow: boolean = wiki.isShadowTiddler(href)
    const classes: string[] = ['tc-tiddlylink']
    if (isShadow) {
        classes.push('tc-tiddlylink-shadow')
    }
    if (isMissing && !isShadow) {
        classes.push('tc-tiddlylink-missing')
    } else {
        if (!isMissing) {
            classes.push('tc-tiddlylink-resolves')
        }
    }
    return classes
}

/**
 * Handle clicking a PrettyLink.
 * Links will only be opened if the editor is in read-only mode
 * or if openWhenEditing is set to True. This allows opening the
 * LinkBubbleMenu when editing links without also following the link.
 * This will check if the link is external or internal and either
 * open the link or open the Tiddler.
 */
export function handlePrettyLinkClick(
    widget: any,
    event: Event,
    openWhenEditing: boolean
) {
    const link = (event.target as HTMLElement)?.closest('a')
    let href = null
    if (link) {
        href = link.getAttribute('href')
    }

    if (href && isExternal(href)) {
        if (openWhenEditing) {
            window.open(href)
        }
        event.preventDefault()
    } else {
        if (openWhenEditing) {
            handleInternalClick(widget, event)
        }
    }

    return true
}

/**
 * This function will call TiddlyWiki to handle the link routing.
 * Use this for clicks to internal pages.
 */
function handleInternalClick(widget: Widget, event: any) {
    if (!widget) {
        return
    }
    const target = event.target
    const link = target.getAttribute('href')
    let storyTiddler = ''
    let fromNode = {}
    storyTiddler = widget.getVariable('storyTiddler')
    fromNode = widget

    const bounds = target.getBoundingClientRect()
    const fromPageRect = {
        top: bounds.top,
        left: bounds.left,
        width: bounds.width,
        right: bounds.right,
        bottom: bounds.bottom,
        height: bounds.height,
    }

    // This event structure is expected by TiddlyWiki's dispatchEvent
    // that is going to be called to allow TiddlyWiki to handle
    // the internal link opening.
    const navEvent = {
        type: 'tm-navigate',
        navigateTo: link,
        navigateFromTitle: storyTiddler,
        navigateFromNode: fromNode,
        navigateFromClientRect: fromPageRect,
        navigateSuppressNavigation:
            event.metaKey || event.ctrlKey || event.button === 1,
        metaKey: event.metaKey,
        ctrlKey: event.ctrlKey,
        altKey: event.altKey,
        shiftKey: event.shiftKey,
    }
    // TiddlyWiki will handle the internal link navigation
    widget.dispatchEvent(navEvent)
}

export const PrettyLink = Mark.create<PrettyLinkOptions>({
    name: 'prettyLink',
    priority: 1000,
    inclusive: false,
    defaultOptions: {
        tw: null,
        wiki: null,
        widget: null,
        openOnClick: true,
        linkOnPaste: true,
        HTMLAttributes: {
            target: '_blank',
            rel: 'noopener noreferrer nofollow',
        },
    },

    addAttributes() {
        return {
            href: {
                default: null,
            },
            target: {
                default: this.options.HTMLAttributes.target,
            },
            rel: {
                default: null,
            },
            'data-link-type': { default: 'prettyLink' },
            class: {
                default: '',
                renderHTML: (attributes) => {
                    return {
                        class: buildClassList(
                            this.options.wiki,
                            attributes.href
                        ).join(' '),
                    }
                },
            },
        }
    },

    parseHTML() {
        return [{ tag: 'a[href][data-link-type="prettyLink"]' }]
    },

    renderHTML({ HTMLAttributes }) {
        return [
            'a',
            mergeAttributes(this.options.HTMLAttributes, HTMLAttributes),
            0,
        ]
    },

    addCommands() {
        return {
            setPrettyLink:
                (attributes) =>
                ({ commands }) => {
                    return commands.setMark('prettyLink', attributes)
                },
            togglePrettyLink:
                (attributes) =>
                ({ commands }) => {
                    return commands.toggleMark('prettyLink', attributes)
                },
            unsetPrettyLink:
                () =>
                ({ commands }) => {
                    return commands.unsetMark('prettyLink')
                },
        }
    },

    addInputRules() {
        return [
            markInputRule({
                find: inputRegex,
                type: this.type,
                getAttributes: (match: string[]) => ({
                    href: match.length === 3 ? match[2] : match[1],
                }),
            }),
        ]
    },

    addPasteRules() {
        return [
            markPasteRule({
                find: pasteRegex,
                type: this.type,
                getAttributes: (match: string[]) => ({
                    href: match[0],
                }),
            }),
        ]
    },

    addProseMirrorPlugins() {
        const plugins = []

        if (this.options.openOnClick) {
            plugins.push(
                new Plugin({
                    key: new PluginKey('handleClickPrettyLink'),
                    props: {
                        handleClick: (view, pos, event) =>
                            handlePrettyLinkClick(
                                this.options.widget,
                                event,
                                false
                            ),
                    },
                })
            )
        }

        if (this.options.linkOnPaste) {
            plugins.push(
                new Plugin({
                    key: new PluginKey('handlePasteLink'),
                    props: {
                        handlePaste: (view, event, slice) => {
                            const { state } = view
                            const { selection } = state
                            const { empty } = selection

                            if (empty) {
                                return false
                            }

                            let textContent = ''

                            slice.content.forEach((node) => {
                                textContent += node.textContent
                            })

                            if (
                                !textContent ||
                                !textContent.match(pasteRegexExact)
                            ) {
                                return false
                            }

                            this.editor.commands.setMark(this.type, {
                                href: textContent,
                            })

                            return true
                        },
                    },
                })
            )
        }

        return plugins
    },
})
