/**
 * TipTap extension that contains macro definitions to be used by
 * TwBlocks within the same editor instance.
 */
import { mergeAttributes, Node } from '@tiptap/core'

declare module '@tiptap/core' {
    interface Commands<ReturnType> {
        macrodefinition: {
            setMacroDefinition: () => ReturnType
        }
    }
}

export const MacroDefinition = Node.create({
    name: 'macrodefinition',
    defaultOptions: { editable: true },
    priority: 1000,
    group: 'block',
    code: true,
    content: 'text*',
    defining: true,

    parseHTML() {
        return [{ tag: 'pre[data-type="macro-definition"]' }]
    },

    renderHTML({ HTMLAttributes }) {
        const hideReadonly: any = {}
        // Do not display the markup if we're read-only
        if (this.options.editable === false) {
            const dom: any = document.createTextNode('')
            // TipTap expects this on the return value
            dom.hasAttribute = () => true
            return dom
        }
        return [
            'pre',
            mergeAttributes(hideReadonly, { 'data-type': 'macro-definition' }),
            0,
        ]
    },

    addCommands() {
        return {
            setMacroDefinition:
                () =>
                ({ commands }) => {
                    return commands.setNode('macrodefinition')
                },
        }
    },

    addAttributes() {
        return {
            name: {
                default: null,
            },
            body: {
                default: '',
            },
            params: {
                default: [],
            },
        }
    },

    addKeyboardShortcuts() {
        return {
            // remove block when at start of document or code block is empty
            Backspace: () => {
                const { empty, $anchor } = this.editor.state.selection
                const isAtStart = $anchor.pos === 1

                if (!empty || $anchor.parent.type.name !== this.name) {
                    return false
                }

                if (isAtStart || !$anchor.parent.textContent.length) {
                    return this.editor.commands.clearNodes()
                }

                return false
            },
        }
    },
})
