import { TW, TwNode, Wiki } from '@/types/tiddlywikitypes'

export function renderTwNode($tw: TW, node: TwNode) {
    // Import all Tiddlers that have the macro tag to get global macros.
    // This came from the default view template $:/core/ui/ViewTemplate
    const wrapperNode = {
        type: 'importvariables',
        attributes: {
            filter: {
                value: '[all[shadows+tiddlers]tag[$:/tags/Macro]!has[draft.of]]',
            },
        },
        // Set default view template variables
        children: [
            {
                type: 'vars',
                attributes: {
                    storyTiddler: {
                        type: 'macro',
                        value: { name: 'currentTiddler', params: [] },
                    },
                    tiddlerInfoState: {
                        type: 'macro',
                        value: {
                            name: 'qualify',
                            params: [
                                {
                                    name: 'title',
                                    value: '$:/state/popup/tiddler-info',
                                },
                            ],
                        },
                    },
                },
                children: [node],
            },
        ],
    }

    const fkParser = { tree: [wrapperNode] }
    const widgetNode = $tw.wiki.makeWidget(fkParser)

    // TW fakeDocument does some cleanup of the content.
    const fkEl = $tw.fakeDocument.createElement('div')
    widgetNode.render(fkEl, null)
    return fkEl.innerHTML
}

export function getLocalMacroDefinitions(docNodes: any) {
    let macroDefinitionText = ''
    // Find macro definition nodes at the beginning of the document.
    // Macro definitions must be the first thing in the pre-amble.
    for (const docNode of docNodes) {
        if (docNode.type === 'macrodefinition') {
            for (const contentNode of docNode.content) {
                macroDefinitionText += `${contentNode.text}\n`
            }
        } else if (docNode.type.name === 'macrodefinition') {
            for (const contentNode of docNode.content.content) {
                macroDefinitionText += `${contentNode.text}\n`
            }
        } else {
            // Only macro definitions at the start of the document are used.
            break
        }
    }
    return macroDefinitionText
}

/**
 * Render TiddlyWikiText as HTML.
 */
export function renderTwWikitext(
    wiki: Wiki,
    macroDefinitions: string,
    wikitext: string
): string {
    let preamble = macroDefinitions

    // Import global macros as part of rendering
    preamble +=
        '<$importvariables filter="[all[shadows+tiddlers]tag[$:/tags/Macro]!has[draft.of]]">\n' +
        '<$importvariables filter="[all[shadows+tiddlers]tag[$:/tags/Macro/View]!has[draft.of]]">\n'

    const postamble = '\n</$importvariables>\n</$importvariables>'
    //     const preamble = `\\define folded-state()
    // $:/state/folded/$(currentTiddler)$
    // \\end
    // \\define cancel-delete-tiddler-actions(message) <$action-sendmessage $message="tm-$message$-tiddler"/>
    // \\import [all[shadows+tiddlers]tag[$:/tags/Macro/View]!has[draft.of]]
    // <$vars storyTiddler=<<currentTiddler>> tiddlerInfoState=<<qualify "$:/state/popup/tiddler-info">>><div data-tiddler-title=<<currentTiddler>> data-tags={{!!tags}} class={{{ tc-tiddler-frame tc-tiddler-view-frame [<currentTiddler>is[tiddler]then[tc-tiddler-exists]] [<currentTiddler>is[missing]!is[shadow]then[tc-tiddler-missing]] [<currentTiddler>is[shadow]then[tc-tiddler-exists tc-tiddler-shadow]] [<currentTiddler>is[shadow]is[tiddler]then[tc-tiddler-overridden-shadow]] [<currentTiddler>is[system]then[tc-tiddler-system]] [{!!class}] [<currentTiddler>tags[]encodeuricomponent[]addprefix[tc-tagged-]] +[join[ ]] }}}><$list filter="[all[shadows+tiddlers]tag[$:/tags/ViewTemplate]!has[draft.of]]" variable="listItem"><$transclude tiddler=<<listItem>>/></$list>
    // </div>
    // </$vars>
    //
    // `
    const wikitextHtml = wiki.renderText(
        'text/html',
        'text/vnd.tiddlywiki',
        preamble + wikitext + postamble
    )
    return wikitextHtml
}

/**
 * Return TiddlyWikiText for editing in a TwBlock.
 */
export function flattenToMarkup(node: any): string {
    // The node content could be an array or tree of text nodes that
    // needs to be flattened into a single string.
    const parts: string[] = []
    const recursiveTexts = (subNode: any) => {
        if (!subNode.content) {
            return
        }
        if (subNode.content.content) {
            recursiveTexts(subNode.content)
        } else if (
            Object.prototype.toString.call(subNode.content) === '[object Array]'
        ) {
            for (const contentNode of subNode.content) {
                if (contentNode.text) {
                    parts.push(contentNode.text)
                }
                recursiveTexts(contentNode)
            }
        }
    }
    recursiveTexts(node)
    return parts.join('\n')
}
