/**
 * The bridge allows getting data from a Tiddler in a Javascript
 * structure.
 */
import { WikiTextParser } from './parser/wikitextparser'

export class BaseWidgetBridge {
    getStructuredData(): any {
        return {}
    }

    save(data: any) {
        // Override in subclass
    }
}

export class WidgetBridge extends BaseWidgetBridge {
    $tw: any
    wiki: any
    widget: any

    constructor($tw: any, wiki: any, widget: any) {
        super()
        this.$tw = $tw
        this.wiki = wiki
        this.widget = widget
    }

    /**
     * Returns data in the TipTap format necessary for the editor.
     * Either from data saved on the Tiddler or by parsing the WikiText
     * into the appropriate structure.
     */
    getStructuredData() {
        // Check if the widget already has structured data saved
        let data = this.widget.getStructuredData()
        if (!data) {
            // Need to parse the WikiText to get structured data
            const tiddler = this.wiki.getTiddler(this.widget.editTitle)
            if (tiddler && tiddler.fields[this.widget.editField]) {
                const parser = new WikiTextParser({ wiki: this.wiki })
                const wikiText = tiddler.fields[this.widget.editField]
                if (wikiText.trim() === '<$prosary/>') {
                    console.log(
                        'WikiText contains Prosary widget, but is missing structured data.'
                    )
                    return null
                }
                data = parser.parseWikiText(wikiText)
                this.save(data)
            }
        }
        return data
    }

    save(data: any) {
        this.widget.save(data)
    }
}

export class MockWidgetBridge extends BaseWidgetBridge {
    data: any
    constructor() {
        super()
        this.data = null
    }

    getStructuredData(): any {
        return this.data
    }

    save(data: any): void {
        this.data = data
    }
}
