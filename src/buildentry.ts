import { createApp } from 'vue'
import App from './App.vue'

// export const app = createApp(App)
export const app = () => createApp(App)
